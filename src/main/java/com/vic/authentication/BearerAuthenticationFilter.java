package com.vic.authentication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * Фильтр для обработки авторизации
 */
@Slf4j
public class BearerAuthenticationFilter extends GenericFilterBean {

    private final AuthenticationManager authenticationManager;

    /**
     * Конструктор
     * @param authenticationManager - аутентификационный менеджер
     */
    public BearerAuthenticationFilter(AuthenticationManager authenticationManager) {
        Assert.notNull(authenticationManager, "authenticationManager cannot be null");
        this.authenticationManager = authenticationManager;
    }

    /**
     * Метод обрабатывает запрос от клиента и пытается аутентифицировать клиента
     * @param request - запрос
     * @param response - ответ
     * @param chain - цепочка фильтров
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String header = httpRequest.getHeader("Authorization");
        if (!StringUtils.hasText(header) || !header.toLowerCase().startsWith("bearer ")) {
            chain.doFilter(request, response);
            return;
        }
        String token = header.substring(7);
        log.debug("Authorization token = {}", token);
        try {
            UUID tokenUuid;
            try {
                tokenUuid = UUID.fromString(token);
            } catch (IllegalArgumentException e) {
                throw new BadCredentialsException("Bad token");
            }
            Authentication authentication = authenticationManager.authenticate(new TokenAuthentication(tokenUuid));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            log.debug("Authentication success");
            chain.doFilter(request, response);
        } catch (AuthenticationException e) {
            SecurityContextHolder.clearContext();
            log.debug("Authentication failed");
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}

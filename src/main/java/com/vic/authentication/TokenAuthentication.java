package com.vic.authentication;

import lombok.Getter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.UUID;

/**
 * Класс объекта аутентификации. Хранит детальную информацию об авторизированном пользователе (хранится в
 * SecurityContextHolder)
 */
public class TokenAuthentication implements Authentication {
    private static final long serialVersionUID = 8582788304276564490L;

    @Getter
    private UUID token;
    private boolean authenticated;

    /**
     * Конструктор
     * @param token - идентификатор абонента
     */
    public TokenAuthentication(UUID token) {
        this.token = token;
        authenticated = false;
    }

    /**
     * Метод возвращает роли пользователя
     * @return список ролей
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    /**
     * Метод возвращает пароль
     * @return пароль
     */
    @Override
    public Object getCredentials() {
        return null;
    }

    /**
     * Метод возвращает дополнительную информацию
     * @return дополнительная информация
     */
    @Override
    public Object getDetails() {
        return null;
    }

    /**
     * Метод возвращает данные пользователя
     * @return данные пользователя
     */
    @Override
    public Object getPrincipal() {
        return null;
    }

    /**
     * Метод возвращает признак аутентификации
     * @return true - пользователь аутентифицирован, false - нет
     */
    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    /**
     * Метод устанавливает признак аутентификации
     * @param isAuthenticated - признак аутентификации
     * @throws IllegalArgumentException
     */
    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }

    /**
     * Метод возвращает имя пользователя
     * @return имя пользователя
     */
    @Override
    public String getName() {
        return null;
    }
}

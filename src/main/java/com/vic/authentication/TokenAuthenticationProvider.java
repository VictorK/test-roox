package com.vic.authentication;

import com.vic.model.entity.Customer;
import com.vic.service.CustomerService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.Objects;
import java.util.UUID;

/**
 * Аутентификационный провайдер. Вызывается аутентификационным менеджером во время аутентификации юзера
 */
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final CustomerService service;

    /**
     * Конструктор
     * @param service - сервис абонента
     */
    public TokenAuthenticationProvider(CustomerService service) {
        this.service = service;
    }

    /**
     * Функция аутентификации пользователя
     * @param authentication - объект аутентификации
     * @return объект аутентификации
     * @throws AuthenticationException - выкидывается в случае неуспешной аутентификации
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UUID token = ((TokenAuthentication) authentication).getToken();
        Customer customer = service.getCustomer(token);
        if (Objects.isNull(customer)) {
            throw new BadCredentialsException("Invalid token");
        }
        authentication.setAuthenticated(true);
        return authentication;
    }

    /**
     * Метод определяет поддерживает ли провайдер указанный класс объекта аутентификации или нет
     * @param authentication - класс объекта аутентификации
     * @return true - поддерживает, false - не поддерживает
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(TokenAuthentication.class);
    }
}

package com.vic.common;

import com.vic.dto.CustomerDto;
import com.vic.dto.CustomerWithoutPasswordDto;
import com.vic.dto.PartnerMappingDto;
import com.vic.model.entity.Customer;
import com.vic.model.entity.PartnerMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Вспомогательный класс для конвертирования объектов ОМ в ДТО объекты
 */
public class ConverterDto {

    /**
     * Конвертирует объект Customer в объект CustomerDto
     * @param customer - объект ОМ
     * @return объект ДТО
     */
    public static CustomerDto convertToDto(Customer customer) {
        return new CustomerDto(customer.getId(), customer.getFio(), customer.getBalance(), customer.getStatus(),
                customer.getLogin(), customer.getPassword());
    }

    /**
     * Конвертирует список Customer в список CustomerWithoutPasswordDto
     * @param list - список объектов ОМ
     * @return список объектов ДТО
     */
    public static List<CustomerWithoutPasswordDto> convertCustomersToWithoutPasswordDto(List<Customer> list) {
        List<CustomerWithoutPasswordDto> result = new ArrayList<>();
        for (Customer customer: list) {
            result.add(convertToWithoutPasswordDto(customer));
        }
        return result;
    }

    /**
     * Конвертирует объект Customer в объект CustomerWithoutPasswordDto
     * @param customer - объект ОМ
     * @return объект ДТО
     */
    public static CustomerWithoutPasswordDto convertToWithoutPasswordDto(Customer customer) {
        return new CustomerWithoutPasswordDto(customer.getId(), customer.getFio(), customer.getBalance(), customer.getStatus(),
                customer.getLogin());
    }

    /**
     * Конвертирует объект PartnerMapping в объект PartnerMappingDto
     * @param partnerMapping - объект ОМ
     * @return объект ДТО
     */
    public static PartnerMappingDto convertToDto(PartnerMapping partnerMapping) {
        return new PartnerMappingDto(partnerMapping.getId(), partnerMapping.getPartnerId(),
                partnerMapping.getPartnerUserId(), partnerMapping.getFio(), partnerMapping.getAvatar());
    }

    /**
     * Конвертирует список PartnerMapping в список PartnerMappingDto
     * @param list - список объектов ОМ
     * @return список объектов ДТО
     */
    public static List<PartnerMappingDto> convertPartnerMappingsToDto(List<PartnerMapping> list) {
        List<PartnerMappingDto> result = new ArrayList<>();
        for (PartnerMapping partnerMapping: list) {
            result.add(convertToDto(partnerMapping));
        }
        return result;
    }

    /**
     * Приватный конструктор, чтоб запретить создание экземпляров данного класса
     */
    private ConverterDto() {
    }
}

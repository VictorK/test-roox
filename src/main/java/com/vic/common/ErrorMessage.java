package com.vic.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Класс хранит информации об ошибке. Используется в REST API
 */
@Getter
@AllArgsConstructor
public class ErrorMessage {
    private String errorMessage;
}

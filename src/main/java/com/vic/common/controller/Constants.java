package com.vic.common.controller;

/**
 * Класс содержит константы, которые используются в контроллерах
 */
public class Constants {

    public static final String AUTHENTICATED_USER_MARKER = "@me";

    /**
     * Приватный конструктор, чтоб запретить создание экземпляров данного класса
     */
    private Constants() {
    }
}

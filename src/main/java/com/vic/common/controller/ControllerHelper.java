package com.vic.common.controller;

import com.vic.authentication.TokenAuthentication;
import com.vic.exception.AccessDeniedException;
import com.vic.exception.IncorrectRequestData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;
import java.util.UUID;

import static com.vic.common.controller.Constants.AUTHENTICATED_USER_MARKER;

/**
 * Вспомогательный класс. Содержит методы используемые в контроллере
 */
@Slf4j
public class ControllerHelper {

    /**
     * Метод преобразовывает идентификатор абонента в UUID формат. Также распознаёт маркер @me, в этом случае берётся
     * идентификатор аутентифицированного обонента абонента
     * @param value - идентификатор абонента в String формате, достаётся из URL
     * @return идентификатор абонента в UUID формате
     */
    public static UUID getCustomerId(String value) {
        UUID result;
        if (AUTHENTICATED_USER_MARKER.equals(value)) {
            result = getAuthenticatedUserToken();
        } else {
            result = convertToUuid(value, "Incorrect customer id");
        }
        return result;
    }

    /**
     * Функция пытается конвертнуть идентификатор из String в UUID, в случае выкидывается исключение с заданным
     * сообщением
     * @param id - идентификатор в String формате
     * @param errorMessage - сообщение об ошибке
     * @return идентификатор в UUID формате
     */
    public static UUID convertToUuid(String id, String errorMessage) {
        UUID result;
        try {
            result = UUID.fromString(id);
            return result;
        } catch (IllegalArgumentException e) {
            throw new IncorrectRequestData(String.format("%s (%s)", errorMessage, id));
        }
    }

    /**
     * Проверяет, есть ли доступ к заданному идентификатору абонента аутентифицированным абонентом. В случае отсутствия
     * доступа - выкидается рантайм исключение
     * @param customerToken - идентификатор абонента к которому получается доступ
     */
    public static void checkAccess(UUID customerToken) {
        UUID userToken = getAuthenticatedUserToken();
        if (!Objects.equals(userToken, customerToken)) {
            String errorMessage = String.format("Access denied for user with token '%s'", userToken.toString());
            log.warn(errorMessage);
            throw new AccessDeniedException(errorMessage);
        }
    }

    /**
     * Функция получает идентификатор аутентифицированного абонента
     * @return идентификатор абонента
     */
    private static UUID getAuthenticatedUserToken() {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) SecurityContextHolder.getContext()
                .getAuthentication();
        return tokenAuthentication.getToken();
    }

    /**
     * Приватный конструктор, чтоб запретить создание экземпляров данного класса
     */
    private ControllerHelper() {
    }
}

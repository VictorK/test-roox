package com.vic.controller;

import com.vic.dto.CustomerDto;
import com.vic.dto.CustomerWithoutPasswordDto;
import com.vic.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static com.vic.common.controller.ControllerHelper.checkAccess;
import static com.vic.common.controller.ControllerHelper.getCustomerId;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Контроллер для работы с абонентом
 */
@RestController
@RequestMapping(value = "/api/customer")
public class CustomerController {

    private final CustomerService service;

    /**
     * Конструктор
     * @param service - сервис абонента
     */
    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    /**
     * Получить список абонентов
     * @return список абонентов
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<CustomerWithoutPasswordDto> getList() {
        return service.getList();
    }

    /**
     * Найти абонента с заданным идентификатором
     * @param id - идентификатор абонента
     * @return абонент
     */
    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public CustomerDto getCustomer(@PathVariable("id") String id) {
        UUID customerUuid = getCustomerId(id);
        checkAccess(customerUuid);
        return service.getCustomerDto(customerUuid);
    }
}

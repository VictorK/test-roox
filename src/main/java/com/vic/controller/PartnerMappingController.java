package com.vic.controller;

import com.vic.dto.PartnerMappingDto;
import com.vic.service.PartnerMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.vic.common.controller.ControllerHelper.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Контроллер для работы с привязкой к партнёрскому сервису
 */
@RestController
@RequestMapping(value = "/api/customer/{customer_id}/partner_mapping")
public class PartnerMappingController {

    private final PartnerMappingService service;

    /**
     * Конструктор
     * @param service - сервис привязки к партнёрскому сервису
     */
    @Autowired
    public PartnerMappingController(PartnerMappingService service) {
        this.service = service;
    }

    /**
     * Получить все привязки к партнёрскому сервису для заданного абонента
     * @param customerId - идентификатор абонента
     * @return список привязок к партнёрскому сервису
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<PartnerMappingDto> getList(@PathVariable("customer_id") String customerId) {
        UUID customerUuid = getCustomerId(customerId);
        checkAccess(customerUuid);
        return service.getList(customerUuid);
    }

    /**
     * Получение привязки к партнёрскому сервису по заданному идентификатору
     * @param customerId - идентификатор абонента
     * @param partnerMappingId - идентификатор привязки к партнёрскому сервису
     * @return привязка к партнёрскому сервису
     */
    @GetMapping(value = "/{partner_mapping_id}", produces = APPLICATION_JSON_VALUE)
    public PartnerMappingDto getPartnerMapping(@PathVariable("customer_id") String customerId,
                                               @PathVariable("partner_mapping_id") String partnerMappingId) {
        checkAccess(getCustomerId(customerId));
        return service.getPartnerMapping(convertToUuid(partnerMappingId, "Incorrect partner mapping id"));
    }

    /**
     * Сохранить привязку к партнёрскому сервису
     * @param customerId - идентификатор абонента
     * @param partnerMapping - привязка к партнёрскому сервису
     * @return сохранённая привязка к партнёрскому сервису
     */
    @ResponseStatus(value = CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public PartnerMappingDto savePartnerMapping(@PathVariable("customer_id") String customerId,
                                                @RequestBody PartnerMappingDto partnerMapping) {
        UUID customerUuid = getCustomerId(customerId);
        checkAccess(customerUuid);
        return service.savePartnerMapping(customerUuid, partnerMapping);
    }

    /**
     * Изменить привязку к партнёрскому сервису
     * @param customerId - идентификатор абонента
     * @param partnerMapping - привязка к партнёрскому сервису
     * @return изменённая привязки к партнёрскому сервису
     */
    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public PartnerMappingDto editPartnerMapping(@PathVariable("customer_id") String customerId,
                                                @RequestBody PartnerMappingDto partnerMapping) {
        checkAccess(getCustomerId(customerId));
        return service.editPartnerMapping(partnerMapping);
    }

    /**
     * Удалить привязку к партнёрскому сервису
     * @param customerId - идентификатор абонента
     * @param partnerMappingId - идентификатор привязки к партнёрскому сервису
     * @return удалённая привязка к партнёрскому сервису
     */
    @DeleteMapping(value = "/{partner_mapping_id}", produces = APPLICATION_JSON_VALUE)
    public PartnerMappingDto deletePartnerMapping(@PathVariable("customer_id") String customerId,
                                                  @PathVariable("partner_mapping_id") String partnerMappingId) {
        checkAccess(getCustomerId(customerId));
        return service.deletePartnerMapping(convertToUuid(partnerMappingId, "Incorrect partner mapping id"));
    }
}

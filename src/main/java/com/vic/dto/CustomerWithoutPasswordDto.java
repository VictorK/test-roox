package com.vic.dto;

import com.vic.model.CustomerStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Класс ДТО хранит информации об абоненте (без поля пароль). Используется в REST API
 */
@Data
@AllArgsConstructor
public class CustomerWithoutPasswordDto {
    private UUID id;
    private String fio;
    private BigDecimal balance;
    private CustomerStatus status;
    private String login;
}

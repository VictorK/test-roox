package com.vic.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * Класс ДТО хранит информации о привязке к партнёрскому сервису. Используется в REST API
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartnerMappingDto {
    private UUID id;
    private UUID partnerId;
    private UUID partnerUserId;
    private String fio;
    private String avatar;
}

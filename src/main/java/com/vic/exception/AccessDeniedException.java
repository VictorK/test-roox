package com.vic.exception;

/**
 * Класс ошибки бросаемый во время нарушения доступа к ресурсу
 */
public class AccessDeniedException extends RuntimeException {
    /**
     * Конструктор
     * @param message - сообщение об ошибке
     */
    public AccessDeniedException(String message) {
        super(message);
    }
}

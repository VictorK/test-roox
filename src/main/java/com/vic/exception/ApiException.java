package com.vic.exception;

/**
 * Класс ошибки бросаемый во время обработки выполнения REST API
 */
public class ApiException extends RuntimeException {
    /**
     * Конструктор
     * @param message - сообщение об ошибке
     */
    public ApiException(String message) {
        super(message);
    }
}

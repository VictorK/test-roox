package com.vic.exception;

/**
 * Класс ошибки бросаемый во время обработки неверный данных, пришедших из запроса
 */
public class IncorrectRequestData extends RuntimeException {
    /**
     * Конструктор
     * @param message - сообщение об ошибке
     */
    public IncorrectRequestData(String message) {
        super(message);
    }
}

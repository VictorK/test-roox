package com.vic.exception.handler;

import com.vic.common.ErrorMessage;
import com.vic.exception.AccessDeniedException;
import com.vic.exception.ApiException;
import com.vic.exception.IncorrectRequestData;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.*;

/**
 * Глобальный хендлер для обработки REST API ошибок
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Обработка сообщений с кодом 404
     * @param e - возникшее исключение
     * @return сообщение об ошибке
     */
    @ResponseStatus(value = NOT_FOUND)
    @ExceptionHandler(value = ApiException.class)
    @ResponseBody
    public ErrorMessage handleApiException(Exception e) {
        return new ErrorMessage(e.getMessage());
    }

    /**
     * Обработка сообщений с кодом 403
     * @param e - возникшее исключение
     * @return сообщение об ошибке
     */
    @ResponseStatus(value = FORBIDDEN)
    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseBody
    public ErrorMessage handleAccessDeniedException(Exception e) {
        return new ErrorMessage(e.getMessage());
    }

    /**
     * Обработка сообщений с кодом 400
     * @param e - возникшее исключение
     * @return сообщение об ошибке
     */
    @ResponseStatus(value = BAD_REQUEST)
    @ExceptionHandler(value = IncorrectRequestData.class)
    @ResponseBody
    public ErrorMessage handleIncorrectRequestData(Exception e) {
        return new ErrorMessage(e.getMessage());
    }
}

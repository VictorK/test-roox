package com.vic.model;

/**
 * Статус абонента
 */
public enum CustomerStatus {
    ACTIVE,
    BLOCKED
}

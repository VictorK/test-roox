package com.vic.model.entity;

import com.vic.model.CustomerStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Класс сущности, который используется для хранения информации об абоненте в БД
 */
@Data
@Entity
@Table(name = "customers")
@ToString(exclude = {"partnerMapping"})
@AllArgsConstructor
@NoArgsConstructor
public class Customer implements Serializable {
    private static final long serialVersionUID = 8811275424362478437L;

    @Id
    @Column(nullable = false)
    private UUID id;
    @Column(nullable = false)
    private String fio;
    private BigDecimal balance;
    @Column(nullable = false)
    private CustomerStatus status;
    @Column(nullable = false)
    private String login;
    @Column(nullable = false)
    private String password;
    @OneToMany(mappedBy = "customer")
    private List<PartnerMapping> partnerMapping;
}

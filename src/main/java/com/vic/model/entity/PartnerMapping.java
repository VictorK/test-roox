package com.vic.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Класс сущности, который используется для хранения информации о привязке к партнёрскому сервису в БД
 */
@Data
@ToString(exclude = {"customer"})
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "partner_mapping")
public class PartnerMapping implements Serializable {
    private static final long serialVersionUID = -6215880492923986496L;

    @Id
    @Column(nullable = false)
    private UUID id;
    @Column(name = "partner_id", nullable = false)
    private UUID partnerId;
    @Column(name = "partner_user_id", nullable = false)
    private UUID partnerUserId;
    private String fio;
    private String avatar;
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
}

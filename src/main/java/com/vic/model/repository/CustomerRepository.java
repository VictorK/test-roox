package com.vic.model.repository;

import com.vic.model.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Класс репозитория для работы с сущностью Customer
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, UUID> {
}

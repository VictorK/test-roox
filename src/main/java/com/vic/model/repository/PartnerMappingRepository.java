package com.vic.model.repository;

import com.vic.model.entity.PartnerMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Класс репозитория для работы с сущностью PartnerMapping
 */
@Repository
public interface PartnerMappingRepository extends JpaRepository<PartnerMapping, UUID> {
    /**
     * Метод получает информацию из БД о всех привязках к партнёрскому сервису для заданного абонента
     * @param customerId - идентификатор абонента
     * @return список привязок к партнёрскому сервису
     */
    @Query(value = "select * from public.partner_mapping where customer_id = ?1", nativeQuery = true)
    List<PartnerMapping> findByCustomer(UUID customerId);

    /**
     * Метод сохраняет информацию в БД о привязке к партнёрскому сервису для заданного абонента
     * @param id - идентификатор
     * @param partnerId - идентификатор партнёра
     * @param partnerUserId - идентификатор абонента а партнёрской системе
     * @param fio - ФИО
     * @param avatar - аватарка
     * @param customerId - идентификатор абонента
     */
    @Transactional
    @Modifying
    @Query(value = "insert into public.partner_mapping(id, partner_id, partner_user_id, fio, avatar, customer_id) " +
            "values(?, ?, ?, ?, ?, ?)", nativeQuery = true)
    void save(UUID id, UUID partnerId, UUID partnerUserId, String fio, String avatar, UUID customerId);
}

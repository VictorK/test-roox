package com.vic.service;

import com.vic.dto.CustomerDto;
import com.vic.dto.CustomerWithoutPasswordDto;
import com.vic.exception.ApiException;
import com.vic.model.entity.Customer;
import com.vic.model.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static com.vic.common.ConverterDto.convertCustomersToWithoutPasswordDto;
import static com.vic.common.ConverterDto.convertToDto;

/**
 * Сервис для работы с абонентом
 */
@Slf4j
@Service
public class CustomerService {

    private final CustomerRepository repository;

    /**
     * Конструктор
     * @param repository - репозиторий абонента
     */
    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    /**
     * Получить список абонентов
     * @return список абонентов
     */
    public List<CustomerWithoutPasswordDto> getList() {
        List<CustomerWithoutPasswordDto> result = convertCustomersToWithoutPasswordDto(repository.findAll());
        if (log.isDebugEnabled()) {
            log.debug("Get list of customers:");
            for (CustomerWithoutPasswordDto customer: result) {
                log.debug("{}", customer);
            }
        }
        return result;
    }

    /**
     * Найти абонента с заданным идентификатором
     * @param id - идентификатор абонента
     * @return абонента ДТО
     */
    public CustomerDto getCustomerDto(UUID id) {
        CustomerDto result = convertToDto(getCustomer(id));
        if (log.isDebugEnabled()) {
            log.debug("Get customer with id '{}'", id);
            log.debug("{}", result);
        }
        return result;
    }

    /**
     * Найти абонента с заданным идентификатором
     * @param id - идентификатор абонента
     * @return абонент
     */
    public Customer getCustomer(UUID id) {
        if (Objects.isNull(id)) {
            String errorMessage = String.format("Incorrect customer id");
            log.warn(errorMessage);
            throw new ApiException(errorMessage);
        }
        Optional<Customer> customer = repository.findById(id);
        if (!customer.isPresent()) {
            String errorMessage = String.format("Customer with id '%s' not found", id.toString());
            log.warn(errorMessage);
            throw new ApiException(errorMessage);
        }
        return customer.get();
    }
}

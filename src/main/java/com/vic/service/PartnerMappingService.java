package com.vic.service;

import com.vic.dto.PartnerMappingDto;
import com.vic.exception.ApiException;
import com.vic.model.entity.PartnerMapping;
import com.vic.model.repository.PartnerMappingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.vic.common.ConverterDto.convertPartnerMappingsToDto;
import static com.vic.common.ConverterDto.convertToDto;

/**
 * Сервис для работы с привязкой к партнёрскому сервису
 */
@Slf4j
@Service
public class PartnerMappingService {

    private final PartnerMappingRepository repository;

    /**
     * Конструктор
     * @param repository - репозиторий привязки к партнёрскому сервису
     */
    @Autowired
    public PartnerMappingService(PartnerMappingRepository repository) {
        this.repository = repository;
    }

    /**
     * Получить все привязки к партнёрскому сервису для заданного абонента
     * @param customerId - идентификатор абонента
     * @return список привязок к партнёрскому сервису
     */
    public List<PartnerMappingDto> getList(UUID customerId) {
        List<PartnerMappingDto> result = convertPartnerMappingsToDto(repository.findByCustomer(customerId));
        if (log.isDebugEnabled()) {
            log.debug("Get list of partner mapping:");
            for (PartnerMappingDto partnerMapping: result) {
                log.debug("{}", partnerMapping);
            }
        }
        return result;
    }

    /**
     * Получение привязки к партнёрскому сервису по заданному идентификатору
     * @param id - идентификатор привязки к партнёрскому сервису
     * @return привязка к партнёрскому сервису
     */
    public PartnerMappingDto getPartnerMapping(UUID id) {
        Optional<PartnerMapping> partnerMappingOptional = repository.findById(id);
        if (partnerMappingOptional.isPresent()) {
            PartnerMappingDto result = convertToDto(partnerMappingOptional.get());
            if (log.isDebugEnabled()) {
                log.debug("Get partner mapping with id '{}'", id);
                log.debug("{}", result);
            }
            return result;
        } else {
            String errorMessage = String.format("Partner mapping with id '%s' not found", id.toString());
            log.warn(errorMessage);
            throw new ApiException(errorMessage);
        }
    }

    /**
     * Сохранить привязку к партнёрскому сервису
     * @param customerId - идентификатор абонента
     * @param dto - привязка к партнёрскому сервису
     * @return сохранённая привязка к партнёрскому сервису
     */
    public PartnerMappingDto savePartnerMapping(UUID customerId, PartnerMappingDto dto) {
        PartnerMappingDto result = new PartnerMappingDto(UUID.randomUUID(), dto.getPartnerId(),
                dto.getPartnerUserId(), dto.getFio(), dto.getAvatar());
        repository.save(result.getId(), result.getPartnerId(), result.getPartnerUserId(), result.getFio(),
                result.getAvatar(), customerId);
        if (log.isDebugEnabled()) {
            log.debug("Save partner mapping with id '{}'", result.getId());
            log.debug("{}", result);
        }
        return result;
    }

    /**
     * Изменить привязку к партнёрскому сервису
     * @param dto - привязка к партнёрскому сервису
     * @return изменённая привязки к партнёрскому сервису
     */
    public PartnerMappingDto editPartnerMapping(PartnerMappingDto dto) {
        Optional<PartnerMapping> partnerMappingOptional = repository.findById(dto.getId());
        if (partnerMappingOptional.isPresent()) {
            PartnerMapping partnerMapping = partnerMappingOptional.get();
            partnerMapping.setPartnerId(dto.getPartnerId());
            partnerMapping.setPartnerUserId(dto.getPartnerUserId());
            partnerMapping.setFio(dto.getFio());
            partnerMapping.setAvatar(dto.getAvatar());
            PartnerMappingDto result = convertToDto(repository.save(partnerMapping));
            if (log.isDebugEnabled()) {
                log.debug("Edit partner mapping with id '{}'", result.getId());
                log.debug("{}", result);
            }
            return result;
        } else {
            String errorMessage = String.format("Partner mapping with id '%s' not found", dto.getId().toString());
            log.warn(errorMessage);
            throw new ApiException(errorMessage);
        }
    }

    /**
     * Удалить привязку к партнёрскому сервису
     * @param id - идентификатор привязки к партнёрскому сервису
     * @return удалённая привязка к партнёрскому сервису
     */
    public PartnerMappingDto deletePartnerMapping(UUID id) {
        Optional<PartnerMapping> partnerMappingOptional = repository.findById(id);
        if (partnerMappingOptional.isPresent()) {
            PartnerMapping partnerMapping = partnerMappingOptional.get();
            PartnerMappingDto result = convertToDto(partnerMapping);
            repository.delete(partnerMapping);
            if (log.isDebugEnabled()) {
                log.debug("Delete partner mapping with id '{}'", result.getId());
                log.debug("{}", result);
            }
            return result;
        } else {
            String errorMessage = String.format("Partner mapping with id '%s' not found", id.toString());
            log.warn(errorMessage);
            throw new ApiException(errorMessage);
        }
    }
}

CREATE TABLE IF NOT EXISTS public.customers(
  id uuid NOT NULL,
  fio text NOT NULL,
  balance numeric(19, 2) default 0,
  status integer NOT NULL,
  login text NOT NULL,
  password text NOT NULL,
  CONSTRAINT customers_pk PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS public.partner_mapping(
  id uuid NOT NULL,
  partner_id uuid NOT NULL,
  partner_user_id uuid NOT NULL,
  fio text,
  avatar text,
  customer_id uuid NOT NULL,
  CONSTRAINT partner_mapping_pk PRIMARY KEY(id)
);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO public.customers(id, fio, balance, status, login, password)
VALUES (public.uuid_generate_v4(), 'Иванов Иван Иваныч', 10, 0, 'Ivanich', 'qwerty'),
(public.uuid_generate_v4(), 'Петров Пётр Петрович', 103.50, 0, 'ppp', '1234'),
(public.uuid_generate_v4(), 'Петрова Анастасия Викторовна', 1100.49, 1, 'pav', 'qwe123!');

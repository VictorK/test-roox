package com.vic.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vic.dto.CustomerDto;
import com.vic.dto.CustomerWithoutPasswordDto;
import com.vic.model.CustomerStatus;
import com.vic.model.entity.Customer;
import com.vic.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.vic.common.ConverterDto.convertToDto;
import static com.vic.common.ConverterDto.convertToWithoutPasswordDto;
import static com.vic.common.controller.Constants.AUTHENTICATED_USER_MARKER;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CustomerController.class)
public class CustomerControllerTest {

    private static final UUID USER_TOKEN = UUID.fromString("a2c6a2ea-e9f3-454f-a35b-70c6802c3af0");
    private Customer user;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerService service;

    @Before
    public void setUp() throws Exception {
        user = new Customer();
        user.setId(USER_TOKEN);
        user.setFio("Петров Пётр Петрович");
        user.setBalance(new BigDecimal(10.10));
        user.setStatus(CustomerStatus.ACTIVE);
        user.setLogin("asd123");
        user.setPassword("qwerty");
        when(service.getCustomer(any())).thenReturn(user);
    }

    @Test
    public void getList() throws Exception {
        List<CustomerWithoutPasswordDto> list = new ArrayList<>();
        list.add(convertToWithoutPasswordDto(user));
        list.add(new CustomerWithoutPasswordDto(UUID.randomUUID(), "Петрова Анастасия Викторовна",
                new BigDecimal(1010.10), CustomerStatus.ACTIVE, "abgt5"));
        when(service.getList()).thenReturn(list);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(list)))
                .andReturn();
    }

    @Test
    public void getCustomer() throws Exception {
        CustomerDto dto = convertToDto(user);
        when(service.getCustomerDto(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + USER_TOKEN.toString())
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void getCustomerMe() throws Exception {
        CustomerDto dto = convertToDto(user);
        when(service.getCustomerDto(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + AUTHENTICATED_USER_MARKER)
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void getCustomerAccessDenied() throws Exception {
        CustomerDto dto = convertToDto(user);
        when(service.getCustomerDto(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }
}

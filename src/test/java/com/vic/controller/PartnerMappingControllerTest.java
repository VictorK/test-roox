package com.vic.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vic.dto.PartnerMappingDto;
import com.vic.exception.ApiException;
import com.vic.model.CustomerStatus;
import com.vic.model.entity.Customer;
import com.vic.service.CustomerService;
import com.vic.service.PartnerMappingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.vic.common.controller.Constants.AUTHENTICATED_USER_MARKER;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PartnerMappingController.class)
public class PartnerMappingControllerTest {

    private static final UUID USER_TOKEN = UUID.fromString("a2c6a2ea-e9f3-454f-a35b-70c6802c3af0");
    private Customer user;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PartnerMappingService service;

    @MockBean
    private CustomerService customerService;

    @Before
    public void setUp() throws Exception {
        user = new Customer();
        user.setId(USER_TOKEN);
        user.setFio("Петров Пётр Петрович");
        user.setBalance(new BigDecimal(10.10));
        user.setStatus(CustomerStatus.ACTIVE);
        user.setLogin("asd123");
        user.setPassword("qwerty");
        when(customerService.getCustomer(any())).thenReturn(user);
    }

    @Test
    public void getList() throws Exception {
        List<PartnerMappingDto> list = new ArrayList<>();
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/1.jpg"));
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петров Пётр Петрович", "/home/user/kitty.png"));
        when(service.getList(any())).thenReturn(list);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(list)))
                .andReturn();
    }

    @Test
    public void getListMe() throws Exception {
        List<PartnerMappingDto> list = new ArrayList<>();
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/1.jpg"));
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петров Пётр Петрович", "/home/user/kitty.png"));
        when(service.getList(any())).thenReturn(list);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" +
                AUTHENTICATED_USER_MARKER + "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(list)))
                .andReturn();
    }

    @Test
    public void getListAccessDenied() throws Exception {
        List<PartnerMappingDto> list = new ArrayList<>();
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/1.jpg"));
        list.add(new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петров Пётр Петрович", "/home/user/kitty.png"));
        when(service.getList(any())).thenReturn(list);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }

    @Test
    public void getPartnerMapping() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.getPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void getPartnerMappingMe() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.getPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + AUTHENTICATED_USER_MARKER +
                "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void getPartnerMappingAccessDenied() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.getPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }

    @Test
    public void getPartnerMappingNotFound() throws Exception {
        when(service.getPartnerMapping(any())).thenThrow(ApiException.class);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void savePartnerMapping() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.savePartnerMapping(any(), any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void savePartnerMappingMe() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.savePartnerMapping(any(), any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/customer/" +
                AUTHENTICATED_USER_MARKER + "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void savePartnerMappingAccessDenied() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.savePartnerMapping(any(), any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }

    @Test
    public void editPartnerMapping() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.editPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void editPartnerMappingMe() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.editPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/customer/" +
                AUTHENTICATED_USER_MARKER + "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void editPartnerMappingAccessDenied() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.editPartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }

    @Test
    public void editPartnerMappingNotFound() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.editPartnerMapping(any())).thenThrow(ApiException.class);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/customer/" +  USER_TOKEN.toString() +
                "/partner_mapping")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString())
                .content(objectMapper.writeValueAsString(dto))
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void deletePartnerMapping() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.deletePartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void deletePartnerMappingMe() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.deletePartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/customer/" +
                AUTHENTICATED_USER_MARKER + "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(dto)))
                .andReturn();
    }

    @Test
    public void deletePartnerMappingAccessDenied() throws Exception {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.fromString("81357d3d-d070-459d-b040-354e1349ee59"),
                UUID.randomUUID(), UUID.randomUUID(), "Петрова Анастасия Викторовна", "/home/1.jpg");
        when(service.deletePartnerMapping(any())).thenReturn(dto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/customer/d824566b-d360-4776-bea1-059cc05bdaa7/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isForbidden())
                .andExpect(content().string("{\"errorMessage\":\"Access denied for user with token 'a2c6a2ea-e9f3-454f-a35b-70c6802c3af0'\"}"))
                .andReturn();
    }

    @Test
    public void deletePartnerMappingNotFound() throws Exception {
        when(service.deletePartnerMapping(any())).thenThrow(ApiException.class);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/customer/" + USER_TOKEN.toString() +
                "/partner_mapping/81357d3d-d070-459d-b040-354e1349ee59")
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + USER_TOKEN.toString());
        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andReturn();
    }
}

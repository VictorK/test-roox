package com.vic.service;

import com.vic.dto.CustomerDto;
import com.vic.dto.CustomerWithoutPasswordDto;
import com.vic.exception.ApiException;
import com.vic.model.CustomerStatus;
import com.vic.model.entity.Customer;
import com.vic.model.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.vic.common.ConverterDto.convertCustomersToWithoutPasswordDto;
import static com.vic.common.ConverterDto.convertToDto;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CustomerServiceTest {

    @TestConfiguration
    static class CustomerServiceTestConfiguration {

        @Autowired
        private CustomerRepository repository;

        @Bean
        public CustomerService service() {
            return new CustomerService(repository);
        }
    }

    @Autowired
    private CustomerService service;

    @MockBean
    private CustomerRepository repository;

    @Test
    public void getList() {
        List<Customer> list = new ArrayList<>();
        list.add(new Customer(UUID.randomUUID(), "Петрова Анастасия Викторовна",
                new BigDecimal(1010.10), CustomerStatus.ACTIVE, "abgt5", "pass", null));
        list.add(new Customer(UUID.randomUUID(), "Петров Пётр Петрович",
                new BigDecimal(1020), CustomerStatus.ACTIVE, "petr", "qwerty", null));
        when(repository.findAll()).thenReturn(list);
        List<CustomerWithoutPasswordDto> result = service.getList();
        assertEquals(convertCustomersToWithoutPasswordDto(list), result);
    }

    @Test
    public void getCustomerDto() {
        Customer customer = new Customer(UUID.randomUUID(), "Петрова Анастасия Викторовна",
                new BigDecimal(1010.10), CustomerStatus.ACTIVE, "abgt5", "pass", null);
        when(repository.findById(any())).thenReturn(Optional.of(customer));
        CustomerDto result = service.getCustomerDto(customer.getId());
        assertEquals(convertToDto(customer), result);
    }

    @Test
    public void getCustomer() {
        Customer customer = new Customer(UUID.randomUUID(), "Петрова Анастасия Викторовна",
                new BigDecimal(1010.10), CustomerStatus.ACTIVE, "abgt5", "pass", null);
        when(repository.findById(any())).thenReturn(Optional.of(customer));
        Customer result = service.getCustomer(customer.getId());
        assertEquals(customer, result);
    }

    @Test(expected = ApiException.class)
    public void getCustomerInvalidId() {
        Customer result = service.getCustomer(null);
    }

    @Test(expected = ApiException.class)
    public void getCustomerEmpty() {
        when(repository.findById(any())).thenReturn(Optional.empty());
        service.getCustomer(UUID.randomUUID());
    }
}
package com.vic.service;

import com.vic.dto.PartnerMappingDto;
import com.vic.exception.ApiException;
import com.vic.model.entity.PartnerMapping;
import com.vic.model.repository.PartnerMappingRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.vic.common.ConverterDto.convertPartnerMappingsToDto;
import static com.vic.common.ConverterDto.convertToDto;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PartnerMappingServiceTest {

    @TestConfiguration
    static class PartnerMappingServiceTestConfiguration {

        @Autowired
        private PartnerMappingRepository repository;

        @Bean
        public PartnerMappingService service() {
            return new PartnerMappingService(repository);
        }
    }

    @Autowired
    private PartnerMappingService service;

    @MockBean
    private PartnerMappingRepository repository;

    @Test
    public void getList() {
        List<PartnerMapping> list = new ArrayList<>();
        list.add(new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null));
        list.add(new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петров Пётр Петрович", "/home/user/12.jpg", null));
        when(repository.findByCustomer(any())).thenReturn(list);
        List<PartnerMappingDto> result = service.getList(UUID.randomUUID());
        assertEquals(convertPartnerMappingsToDto(list), result);
    }

    @Test
    public void getPartnerMapping() {
        PartnerMapping partnerMapping = new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null);
        when(repository.findById(any())).thenReturn(Optional.of(partnerMapping));
        PartnerMappingDto result = service.getPartnerMapping(UUID.randomUUID());
        assertEquals(convertToDto(partnerMapping), result);
    }

    @Test(expected = ApiException.class)
    public void getPartnerMappingEmpty() {
        PartnerMapping partnerMapping = new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null);
        when(repository.findById(any())).thenReturn(Optional.empty());
        service.getPartnerMapping(UUID.randomUUID());
    }

    @Test
    public void savePartnerMapping() {
        PartnerMappingDto dto = new PartnerMappingDto(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png");
        PartnerMappingDto result = service.savePartnerMapping(UUID.randomUUID(), dto);
        dto.setId(result.getId());
        assertEquals(dto, result);
    }

    @Test
    public void editPartnerMapping() {
        PartnerMapping partnerMapping = new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null);
        when(repository.findById(any())).thenReturn(Optional.of(partnerMapping));
        when(repository.save(any())).thenReturn(partnerMapping);
        PartnerMappingDto dto = convertToDto(partnerMapping);
        PartnerMappingDto result = service.editPartnerMapping(dto);
        assertEquals(dto, result);
    }

    @Test(expected = ApiException.class)
    public void editPartnerMappingEmpty() {
        PartnerMapping partnerMapping = new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null);
        when(repository.findById(any())).thenReturn(Optional.empty());
        service.editPartnerMapping(convertToDto(partnerMapping));
    }

    @Test
    public void deletePartnerMapping() {
        PartnerMapping partnerMapping = new PartnerMapping(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(),
                "Петрова Анастасия Викторовна", "/home/user/1.png", null);
        when(repository.findById(any())).thenReturn(Optional.of(partnerMapping));
        PartnerMappingDto dto = convertToDto(partnerMapping);
        PartnerMappingDto result = service.deletePartnerMapping(UUID.randomUUID());
        assertEquals(dto, result);    }

    @Test(expected = ApiException.class)
    public void deletePartnerMappingEmpty() {
        when(repository.findById(any())).thenReturn(Optional.empty());
        service.deletePartnerMapping(UUID.randomUUID());
    }
}